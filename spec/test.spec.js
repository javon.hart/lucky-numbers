"use strict";

let Lucky = require("../challenge");

describe("Lucky module", function() {
  it("should find a lucky between 62431 & 62432", function() {
    expect(Lucky.lucky(62431, 62432).length).toEqual(1);
  });
  it("should find lucky numbers between 100 & 200", function() {
  	let truth = [ 106, 115, 124, 133, 142, 151, 160, 169, 178, 187, 196 ];
    expect(Lucky.lucky(100, 200)).toEqual(truth);
  });
  it("should find lucky numbers between 62 & 400", function() {
  	let truth = [70, 79, 88, 97, 106, 115, 124, 133, 142, 151, 160, 169, 178, 187, 196, 205, 214, 223, 232, 241, 250, 259, 268, 277, 286, 295, 304, 313, 322, 331, 340, 349, 358, 367, 376, 385, 394];
    expect(Lucky.lucky(62, 400)).toEqual(truth);
  });
  it("should find lucky numbers between 62 & 500", function() {
  	let truth = [70, 79, 88, 97, 106, 115, 124, 133, 142, 151, 160, 169, 178, 187, 196, 205, 214, 223, 232, 241, 250, 259, 268, 277, 286, 295, 304, 313, 322, 331, 340, 349, 358, 367, 376, 385, 394, 403, 412, 421, 430, 439, 448, 457, 466, 475, 484, 493];
    expect(Lucky.lucky(62, 500)).toEqual(truth);
  });
  it("should find lucky numbers between 999,999,999,900 & 1,000,000,000,000", function() {
  	let truth = [999999999907, 999999999916, 999999999925, 999999999934, 999999999943, 999999999952, 999999999961, 999999999970, 999999999979, 999999999988, 999999999997];
    expect(Lucky.lucky(999999999900, 1000000000000)).toEqual(truth);
  });
});

