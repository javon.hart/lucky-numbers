# Lucky Number Generator

A "lucky number" is one where the digits add up to 7 in successive additions (7 itself also counts as a lucky number). 
This program finds all lucky numbers between arg1 and arg2 ``lucky(1000, 2000)``

## Setup 

`` npm install && npm test ``