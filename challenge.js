"use strict";

let Lucky = (function() {
	let toStrArr = (num) => {
		return num.toString().split('');
	}
	let sum = (num) => {
		return toStrArr(num).reduce((a, b) => parseInt(a) + parseInt(b), 0)
	};
	let lucky = (arg1, arg2) => {
		let luckies = [];
		while (arg2 >= arg1) { // countdown to glory	
			let isLucky = sum(arg2);
			if (isLucky == 7) {
				luckies.push(arg2);
			} else {
				let next = toStrArr(isLucky);
				while (next.length > 1) { // continue addition if possible
					isLucky = sum(next.reduce((a, b) => a + b));
					if (isLucky == 7) {
						luckies.push(arg2);
					}
					next = toStrArr(isLucky);
				}
			}
			arg2--;
		}
		return luckies.sort((a, b) => a - b);
	}
	return {
		lucky: lucky
	};
})();

module.exports = Lucky;

